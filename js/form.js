$('form').submit(function(event) {
	event.preventDefault();
      var name = $("input[name='name']").val();
      var email = $("input[name='email']").val();
      var phone = $("input[name='phone']").val();
      var message = $("textarea[name='message']").val();
      $this = $('input[type]');
      $this.prop("disabled", true);
      console.log(name)
      $.ajax({
        url: 'php/form.php',
        type: 'POST',
        data: {
            name: name,
            email: email,
            phone: phone,
            message: message,
        },
        success: function(data) {
            $('form').trigger('reset');
            $('form textarea').prop('disabled', true);
        },
        error: function(error) {
            console.log(error);
        }
});
})