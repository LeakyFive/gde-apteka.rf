$(document).on('click', '.search-button', function(){
	let value = $('input[type="text"]').val();
	if (value == '') {
		$('input[type="text"]').addClass('search-field-invalid');
		return false
	}
	$.ajax({
		url: 'search.php',
		type: 'GET',
		data: {
			value: value,
		},
		success: function(response) {
			window.location.href = 'search.php?value=' + value;
		},
		error: function(error) {
			console.log(error);
		}
	})
})

$('form').submit(function(e){
    e.preventDefault();
  });

$('input[type="text"]').keypress(function(e) {
	if (e.which == 13) {
		console.log(e);
		$('.search-button').click();
		return false;
	}
})