let value = $('.search-input').val();
var entity;

$.ajax({
        url: 'php/find.php',
        type: 'GET',
        async: false,
        global: false,
        data: {
            value: value,
        },
        success: function(data) {
            entity = JSON.parse(data);
            $.ajax({
        url: 'php/detail.php',
        type: 'GET',
        data: {
            value: value,
        },
        success: function(data) {
            $('.search-results').html(data)      
        },
        error: function(error) {
            console.log(error);
        }
})

        },
        error: function(error) {
            console.log(error);
        }
})



ymaps.ready(init);
    var myMap;

$(document).on('click', 'article p', function() {
    let id = $(this).attr('data-id');
    $.ajax({
        url: 'php/detailModal.php',
        type: 'GET',
        data: {
            id: id,
        },
        success: function(data) {
            let info = JSON.parse(data);
            let modal = $('.modal');            
            $('.pharm-image').attr('src', info['photo_patch']);
            $('.pharm-name').html(info['name']);
            $('.pharm-address').html(info['address']);
            $('.pharm-work-hours').html(info['working_hours']+', '+info['phone']);
            $('.pharm-description').html(info['description']);
            modal.show();
        },
        error: function(error) {
            console.log(error);
        }
});
});


$(document).on('click', '.popup-close', function() {
    $('.modal').hide();
});

$(document).mouseup(function (e) {
    var container = $('.modal');
    if (container.has(e.target).length === 0){
        container.hide();
    }
});

$('.filter-image').click(function() {
    $('.search-results').slideToggle();
})

$('.search-input').keypress(function(e) {
    if (e.which == 13) {
        let value = $('.search-input').val();
        $.ajax({
        url: 'php/find.php',
        type: 'GET',
        data: {
            value: value
        },
    }).done(function(data) {
        objectManager.removeAll();
        objectManager.add(data);
        myMap.setBounds(objectManager.getBounds());
        let value = $('.search-input').val();
        $.ajax({
        url: 'php/detail.php',
        type: 'GET',
        data: {
            value: value,
        },
        success: function(data) {
            $('.search-results').html(data)      
        },
        error: function(error) {
            console.log(error);
        }
});
    });
    }
})

function init() {
	myMap = new ymaps.Map("map", {
            center: [55.76, 37.64], 
            zoom: 7,
            controls: ['geolocationControl']
        }),
        objectManager = new ymaps.ObjectManager({
            clusterize: true,
            gridSize: 32,
            clusterDisableClickZoom: false
        });
    objectManager.objects.options.set({
            iconLayout: 'default#image',
            iconImageHref: 'img/map.png',
            iconImageSize: [30, 42],
            iconImageOffset: [-3, -42]
    });
    objectManager.clusters.options.set('preset', 'islands#redClusterIcons');
    myMap.geoObjects.add(objectManager);

    $.ajax({
        url: 'php/find.php',
        type: 'GET',
        data: {
            value: value
        },
    }).done(function(data) {
        objectManager.add(data);
    });

	ymaps.geolocation.get({
    provider: 'browser',
    mapStateAutoApply: false
}).then(function (result) {
    myMap.setCenter(result.geoObjects.get(0).geometry.getCoordinates(), 16, {duration: 0});
    myMap.geoObjects.add(result.geoObjects);
});
}
