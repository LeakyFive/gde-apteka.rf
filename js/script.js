let arrayOfParameters = [];
$('select').each(function() {
	let parameters = {};
	parameters['filter'] = $(this).attr('data-filter');
	parameters['table'] = $(this).attr('data-table');
	parameters['field'] = $(this).attr('data-sortField')
	parameters['currentValue'] = $('option:selected',this).attr('data-id');
	arrayOfParameters.push(parameters);
});

$('.exit').click(function(){
	$.ajax({
		url: '../log_out.php',
		success: function() {
			window.location = '../../index.php';
		},
		error: function(error) {
			console.log(error);
		}
	})
})

$('select').change(function(){
	let filterInput = $(this).attr('data-filter');
	let sortField = $(this).attr('data-sortField');
	if (typeof(filterInput) != "undefined") {
		let valueOption = $(this).find(':selected').attr('data-id');
		$.each(arrayOfParameters, function() {
			if (this.field == sortField) {
				this.currentValue = valueOption
			}
		})
		$.ajax({
		url: '../filter.php',
		type: 'GET',
		data: {
			data: JSON.stringify(arrayOfParameters)
		},
		success: function(data) {
			$('#refreshData').html(data);
		},
		error: function(error) {
			console.log(error);
		}
	})
	}
});

let entity = {};

$(document).on('click', '.update-button', function() {
	let card = $(this).parent().parent().parent();
	let inputs = card.find('input[disabled]');
	inputs.prop('disabled', false)
	card.find('.icon_button').hide();
	card.find('.update-buttons').show();
});

$(document).on('click', '.delete-button', function() {
	let table = $(this).attr('data-table');
	let action = 'delete';
	let id = {}
	id['id'] = $(this).attr('data-id');
	id['field'] = $(this).attr('data-idfield');
	$.ajax({
		url: '../crud.php',
		type: 'POST',
		data: {
			entity: JSON.stringify(entity),
			table: table,
			action: action,
			id: JSON.stringify(id),
		},
		success: function(response) {
			$('div[data-id='+id['id']+']').remove();
		},
		error: function(error) {
			alert(error);
		}
	})
});

$(document).on('click', '.hideadd-button', function() {
	$("#refreshData").hide();
	$('.filters').hide();
	$("#add").show();
	$('.btn-add').hide();
});
	


$("#formAdd").submit(function(e) {
	e.preventDefault();
	let formFiles = new FormData(this);
	let table = $('.add-button').attr('data-table');
	$.ajax({
		url: '../crud.php?action=add&table='+table,
		type: 'POST',
		data: formFiles,
		cache: false,
        processData: false,
        contentType: false,
		success: function(data) {
			location.reload();
		},
		error: function(error) {
			alert(error);
		}
	})
});


$(document).on('click', '.btn-secondary', function() {
	let card = $(this).parent().parent().parent();
	let inputs = card.find('input');
	inputs.prop('disabled', true)
	card.find('.update-buttons').hide();
	card.find('.icon_button').show();
	entity = {}
});

$(document).on('click', '.ready-button', function() {
	let table = $(this).attr('data-table');
	let action = 'update';
	let card = $(this).parent().parent().parent();
	let inputs = card.find('input');
	let id = {}
	id['id'] = $(this).attr('data-id');
	id['field'] = $(this).attr('data-idfield');
	inputs.each(function() {
		let field = $(this).attr('data-id');
		entity[field] = $(this).val();
	});
	$.ajax({
		url: '../crud.php',
		type: 'POST',
		data: {
			entity: JSON.stringify(entity),
			table: table,
			action: action,
			id: JSON.stringify(id),
		},
		success: function(data) {
		},
		error: function(error) {
			console.log(error);
		}
	})
	inputs.prop('disabled', true)
	card.find('.update-buttons').hide();
	card.find('.icon_button').show();
})