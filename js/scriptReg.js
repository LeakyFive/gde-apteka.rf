$('input[type="checkbox"]').change(function(){
	if ($(this).is(':checked')) {

            $('button').removeAttr('disabled');

        } else {
            $('button').attr('disabled', true);
        }
});

$('form').submit(function(e) {
	e.preventDefault();
  let name = $('input[name="name"]').val();
  let phone = $('input[name="phone"]').val();
  let email = $('input[name="email"]').val();
  let login = $('input[name="login"]').val();
  let password = $('input[name="password"]').val();
  let method = 'reg';
  $.ajax({
   	    url: '../php/API.php',
        type: 'POST',
        data: {
            name: name,
            phone: phone,
            email: email,
            login: login,
            password: password,
            method: method,
        },
        success: function(data) {
        },
        error: function(error) {
            console.log(error);
        }
})
})

var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Пароли не совпадают!");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;