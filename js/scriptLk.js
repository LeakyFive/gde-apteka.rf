$('form button[name="edit"]').click(function(e) {
	e.preventDefault();
	let name = $('input[name="name"]').val();
	let phone = $('input[name="phone"]').val();
	let email = $('input[name="email"]').val();
	let login = $('input[name="login"]').val();
	let password = $('input[name="password"]').val();
	let method = 'update';
	$.ajax({
        url: '../php/API.php',
        type: 'POST',
        data: {
            name: name,
            phone: phone,
            email: email,
            login: login,
            password: password,
            method: method,
        },
        success: function(data) {
            $('.error').html(data);
        },
        error: function(error) {
            console.log(error);
        }
})
})

$('form button[name="exit"]').click(function() {
    $.ajax({
        url: '../php/log_out.php',
        success: function(data) {
            window.location = '../index.php';
        },
        error: function(error) {
            console.log(error);
        }
})
})