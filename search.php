<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Поиск аптек и лекарств</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/searchstyle.css">
	<link href="css/fontawesome-all.css" rel="stylesheet">
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
	<script src="js/jquery-3.3.1.js">
  	</script>
  	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<?php
if (!empty($_GET['value'])) {
	$searchValue = mb_strtolower($_GET['value']);
}
?>
	<header>
		<a class="bigLogo" href="index.html"><img src="img/LogoSearch.png" alt="Где-аптека.рф"/></a>
		<a href="index.php"><img src="img/LogoSearchMini.jpg" alt="Где-аптека.рф"/></a>
		<input class="search-input" type="text" name="search" value="<?php if (!empty($searchValue)) echo $searchValue; ?>" placeholder="Введите название аптеки">
		<div class="user-settings">
			<?php if (isset($_SESSION['id'])) {
				echo "<p class='user-name'>{$_SESSION['login']}</p>
			<a class='settings-icon' href='forms/lk.php'></a>"; } else {
				echo "<a href='forms/login.html'><i class='fas fa-door-open'></i></a>";
				} ?>
		</div>
	</header>
	<main id="map">
		<div id="map-overlay">
		<article class="modal">
			<figure>
				<img class="pharm-image" src="" alt="farm_pic">
				<figcaption>
					<h2 class="pharm-name"></h2>
					<p class="pharm-address"></p>
					<p class="pharm-work-hours"></p>
					<p class="pharm-description"></p>
				</figcaption>				
			<i class="fas fa-times popup-close"></i>
			</figure>
		</article>
			<div id="search-results">
				<div class="filter">
					<p>Результаты поиска</p>
					<a class="filter-image"></a>		
				</div>
				<div class="search-results">
					
				</div>
			</div>
		</div>
	</main>
  	<script src="js/map.js"></script>
  	<script src="js/form.js"></script>
</body>
</html>