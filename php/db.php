<?php
	class ConnectDB {
		private $connect;

		function __construct(){
			$config = parse_ini_file("configurations/config.txt", true);

			$this->connect = new mysqli($config['database-connection']['host'], $config['database-connection']['user'], $config['database-connection']['password'], $config['database-connection']['db_name']);
			if (!$this->connect) exit("Нет соединения с БД");
			$this->connect->set_charset('utf8');
		}

		public function selectFromTwoTables($tableOne, $tableTwo, $field){
			$myselect = "SELECT * FROM $tableOne, $tableTwo WHERE $tableOne.$field=$tableTwo.$field";
			$q=$this->connect->query($myselect);
			if($q) return $q;
			else return null;
		}

		public function select($table) {

			$myselect = "SELECT * FROM $table";
			$q=$this->connect->query($myselect);
			if($q) return $q;
			else return null;	
		}

		public function update($table, $data, $id) {
			$insertQuery = "UPDATE $table SET ";
			foreach ($data as $key => $value) {
				if ($key != 'city' && $key != 'pharm_type' && $key != 'pharmacy_name'){
					$insertQuery .= "$key = '$value',";
				}
			}
			$insertQuery = substr($insertQuery, 0, -1);
			$insertQuery .= " WHERE {$id['field']} = {$id['id']}";
			$q=$this->connect->query($insertQuery);
		}

		public function delete($table, $id) {
			$deleteQuery = "DELETE FROM $table WHERE {$id['field']} = {$id['id']}";
			$q=$this->connect->query($deleteQuery);
			if (!$q) {
    			printf("Errormessage: %s\n", $this->connect->error);
			}
			var_dump($deleteQuery);
			var_dump($q);
		}

		public function add($table, $data){
			var_dump($table);
			if ($table == 'pharmacy') {
				$imgDir = $data['name'];        // каталог для хранения изображений
    			@mkdir('../img/'.$imgDir, 0777); // cсоздаём каталог если его нет (для win машины параметр не обязателен)
 	
 				//die(var_dump(sizeof($_FILES['images']['name'])));
 				$tmp = $_FILES['images']['tmp_name']; //  временное имя файла с путём
				//die(var_dump($_FILES['images']['tmp_name'][$i]));
    
       			// Проверяем, принят ли файл.
     			if (is_uploaded_file($tmp)) {
 		 			$info = getimagesize($tmp);
      				//var_dump($info);
      				// Проверяем, является ли файл изображением.
      			if (preg_match('{image/(.*)}is', $info['mime'], $p)) {


        	 			$name = $_FILES['images']['name']; //можно получить реальное имя так с расширением
        				$src="../img/$imgDir/".$name;
      					$srcInDB = "img/$imgDir/".$name;
         				move_uploaded_file($tmp, $src);// Добавляем файл в каталог с фотографиями.
                
        
      				} 
      			else 
        		die( "<h2>Попытка добавить файл недопустимого формата!</h2>");
      
    			} else  die( "<h2>Ошибка закачки </h2>");
    			$q = "INSERT INTO pharmacy(rating, name, id_city, address, coordX, coordY, phone, description, pharm_id, working_hours, features_id, pharmacy_email) VALUES ('{$data['rating']}', '{$data['name']}', '{$data['id_city']}', '{$data['address']}', '{$data['coordX']}', '{$data['coordY']}', '{$data['phone']}', '{$data['description']}', '{$data['pharm_id']}', '{$data['working_hours']}', '0', '{$data['pharmacy_email']}')";
    			$this->makeQuery($q);
    			$lastId = $this->makeQuery('SELECT MAX(id_pharm) FROM pharmacy');
    			$res = $lastId->fetch_all(MYSQLI_ASSOC);
				$maxId = $res[0]['MAX(id_pharm)'];
    			$qImage = "INSERT INTO photo_pharm(photo_patch, real_name, id_pharm) VALUES ('$srcInDB', '$name', $maxId)";
    			$this->makeQuery($qImage);
			}
			if ($table == 'user') {
				$q = "INSERT INTO users(name, phone, email, login, password, id_type) VALUES ('{$data['name']}', '{$data['phone']}', '{$data['email']}', '{$data['login']}', '{$data['password']}', 2)";
				var_dump($this->makeQuery($q));
			}
		}

		public function sortBy($table, $field, $order) {

			$sortQuery = "SELECT * FROM $table ORDER BY $field $order";
			$q=$this->connect->query($sortQuery);
			if($q) return $q;
			else return null;	
		}
		
		public function makeQuery($myQuery){
			$q=$this->connect->query($myQuery);
			if($q) return $q;
			else return null;
		}

		public function makeInsert($myQuery){
			if (!$this->connect->query($myQuery)) {
				echo $this->error;
			}
		}

		public function get_connection(){
			return $this->connect;
		}
	}