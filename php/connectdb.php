<?php
	$config = parse_ini_file("configurations/config.txt", true);
	$connect = new mysqli($config['database-connection']['host'], $config['database-connection']['user'], $config['database-connection']['password'], $config['database-connection']['db_name']);
	$connect->set_charset('utf8');