<?php

require_once "db.php";
require_once "GeoObject.php";

$searchValue = $_GET['value'];
//$searchValue = 'апт';
$db = new ConnectDB();
$query = "SELECT * FROM pharmacy WHERE lower(name) LIKE lower('%$searchValue%')";
$result = $db->makeQuery($query);
$mydata=$result->fetch_all(MYSQLI_ASSOC);
//var_dump($mydata);

$collection = new FeatureCollection();
$collection->features = array();


for ($i=0; $i < sizeof($mydata); $i++) {
	$features = new Features();
	$features->geometry = new Geometry();
	$features->properties = new Properties();
	$features->id = $mydata[$i]['id_pharm'];
	$features->geometry->coordinates = array();
	$features->properties->balloonContentHeader = "<h2>{$mydata[$i]['name']}</h2>";
	$features->properties->balloonContentBody = "<p>Адрес: {$mydata[$i]['address']}</p><p>{$mydata[$i]['description']}</p>";
	array_push($features->geometry->coordinates, (float)$mydata[$i]['coordX'], (float)$mydata[$i]['coordY']);
	array_push($collection->features, $features);
}

echo json_encode($collection, JSON_UNESCAPED_UNICODE);
