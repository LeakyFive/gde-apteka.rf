<?php
session_start();
	require_once'db.php';
	//require_once'class_token.php';
	require_once'IS.php';

	class API extends ConnectDB {
		use IS;


		function __construct(){
			$this->db = new ConnectDB();
			$this->connection = $this->db->get_connection();
		}

		public function registration(){
			$login=API::checkXSS($_POST['login']);
			$password=API::checkXSS($_POST['password']);
			$phone=API::checkXSS($_POST['phone']);
			$name=API::checkXSS($_POST['name']);
			$email=API::checkXSS($_POST['email']);
			$id_type = 1;

			if (!($preparedStatement = $this->connection->prepare('INSERT INTO users (login,password,phone,name,id_type,email) VALUES (?,?,?,?,?,?)  '))) {
    	die( "Не удалось подготовить запрос: (" . $this->connection->errno . ") " . $this->connection->error);
	}
	$preparedStatement->bind_param("ssssis",$login, $password, $phone,$name,$id_type,$email);
          $preparedStatement->execute();
        
            $result=$preparedStatement->affected_rows;  

if (!$result) die(' Error update in BD'); 
	

else {
	header("Location: ../forms/login.html");
				$this->connection->close();
			}
$preparedStatement->close();	
		}

public function authorization(){
			$login=API::checkXSS($_POST['login']);
			$password=API::checkXSS($_POST['password']);
			
			if (!($preparedStatement = $this->connection->prepare('SELECT `id` FROM users WHERE login = ? AND password = ?'))) {
    			die( "Не удалось подготовить запрос: (" . $this->connection->errno . ") " . $this->connection->error);
			}
			$preparedStatement->bind_param("ss",$login, $password);
         	$a= $preparedStatement->execute();
        
            $result=$preparedStatement->get_result();  
          	$id = $result->fetch_array(MYSQLI_ASSOC)['id'];
			if (!$result) die(' Error in BD'); 
			else {
			$_SESSION['id'] = $id;
			$_SESSION['login'] = $login;
			$this->connection->close();
			$connect = new ConnectDB();
			$id = $_SESSION['id'];
			$rec = $connect->makeQuery("SELECT * FROM users,types WHERE id = '$id' AND types.id_type = users.id_type");
			$data = $rec->fetch_assoc();
			if (empty($data)) {
				echo "Логин или пароль введён неверно";
			}
			if ($data['type'] == 'admin'){
				$_SESSION['type'] = 'admin';
				header("Location: ../php/admin/indexAdmin.php");
			} 
			if (!empty($data)) {
			header("Location: ../forms/lk.php");
			}
			}
			$preparedStatement->close();
}

		public function editUser(){
			$id=$_SESSION['id'];
			$login=API::checkXSS($_POST['login']);
			$phone=API::checkXSS($_POST['phone']);
			$name=API::checkXSS($_POST['name']);
			$email=API::checkXSS($_POST['email']);
			$query = "UPDATE users SET login='$login',phone='$phone',name='$name',email='$email' WHERE id='$id'";

			$q=$this->connection->query($query);

			if (!$q) exit(var_dump($q));
			$this->connection->close();
		}

}
	$user = new API();

	if ($_POST['method'] == "reg") {
		
		if(!$user->checkRegular($_POST['login'])){
			die("Пустой логин");
		}
		if(!$user->checkRegular($_POST['password'])){
			die("Пустой Пароль");
		}
		if(!$user->checkRegular($_POST['phone'])){
			die("Пустой Телефон");
		}
		if(!$user->checkRegular($_POST['name'])){
			die("Пустое Имя");
		}
		if(!$user->checkRegular($_POST['email'])){
			die("Пустой email");
		}


		$user->registration();
	} 

	if ($_POST['method'] == "update") {
		if(!$user->checkRegular($_POST['login'])){
			die("Пустой логин");
		}
		if(!$user->checkRegular($_POST['password'])){
			die("Пустой Пароль");
		}
		if(!$user->checkRegular($_POST['phone'])){
			die("Пустой Телефон");
		}
		if(!$user->checkRegular($_POST['name'])){
			die("Пустое Имя");
		}
		if(!$user->checkRegular($_POST['email'])){
			die("Пустой email");
		}
		$user->editUser();
	} 

	if ($_POST['method'] == "auth") {

		if(!$user->checkRegular($_POST['login'])){
			die("Пустой логин");
		}
		if(!$user->checkRegular($_POST['password'])){
			die("Пустой Пароль");
		}
		$user->authorization();
	}
