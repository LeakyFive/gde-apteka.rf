<?php
   require_once "../db.php";
   session_start();
   ?>
<!DOCTYPE html>
<!--[if It IE 7]<html lang="ru" class ="It-ie8 It-ie7"><![endif]-->
   <!--[if IE 7]<html lang="ru" class ="It-ie9 It-ie8"><![endif]-->
   <!--[if IE 8]<html lang="ru" class ="It-ie9"><![endif]-->
   <!--[if IE 8]><!-->
<html lang="ru">
   <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Панель администратора</title>
      <link rel="shortcut icon" href="../../img/logo.png" type="image/png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- совместимость с IE -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--для адаптивной работы сайта-->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" >
      <link rel="stylesheet" href="../../css/bootstrap.min.css">
      <script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
      <link rel="stylesheet" type="text/css" href="style.css">
   </head>
   <body>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <header>
         <nav class="navbar navbar-default" role="navigation">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle </span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="../../img/logo.png"></a>
               </div>
               <!--Меню-->
               <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-nav">
                     <li><a href="admins.php">Администраторы</a></li>
                     <li><a href="indexAdmin.php">Аптеки</a></li>
                     <li class="active-link"><a href="users.php">Пользователи</a></li>
                     <li><a class="exit">Выйти</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <div class='content' id='content'>
         <div class='container'>
            <form>
               <div class="filters">
                  <select data-table='users' data-sortField="login" data-filter="false" class="form-control">
                     <option data-sortField="login" data-id="ASC"> Логин A-Z </option>
                     <option data-sortField="login" data-id="DESC"> Логин Z-A </option>
                  </select>
               </div>
            </form>
            <div id='refreshData' class='row'>
               <?php
                  $object = new ConnectDB();
                  $res=$object->select("users");
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="1"){
                        echo "
                           <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>
                                    <img class='img img-circle' src='../../img/user-icon.png'>
                                    <table>
                                       <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' data-id='login' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='phone' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='email' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table>
                                 </div>
                                 <div class='update-buttons'>
                                    <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-success ready-button glyphicon glyphicon-ok'></button>
                                    <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-secondary glyphicon glyphicon-remove'></button>
                                 </div>
                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' class='delete-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                              </div>
                           </div>
                        ";
                     }
                  }
               ?>
            </div>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="../../js/bootstrap.min.js"></script>
      <script type="text/javascript" src="../../js/script.js"></script>
      <script src="main.js"></script>
   </body>
</html>