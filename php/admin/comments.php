<?php
   require_once "../db.php";
   session_start();
   ?>
<!DOCTYPE html>
<!--[if It IE 7]<html lang="ru" class ="It-ie8 It-ie7"><![endif]-->
   <!--[if IE 7]<html lang="ru" class ="It-ie9 It-ie8"><![endif]-->
   <!--[if IE 8]<html lang="ru" class ="It-ie9"><![endif]-->
   <!--[if IE 8]><!-->
<html lang="ru">
   <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Панель администратора</title>
      <link rel="shortcut icon" href="../../img/logo.png" type="image/png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- совместимость с IE -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--для адаптивной работы сайта-->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" >
      <link rel="stylesheet" href="../../css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="style.css">
      <script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
   </head>
   <body>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <header>
         <nav class="navbar navbar-default" role="navigation">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle </span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="../../img/logo.png"></a>
               </div>
               <!--Меню-->
               <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-nav">
                     <li><a href="admins.php">Администраторы</a></li>
                     <li><a href="moderators.php">Модераторы</a></li>
                     <li><a href="indexAdmin.php">Аптеки</a></li>
                     <li><a href="drugs.php">Медикаменты</a></li>
                     <li><a href="users.php">Пользователи</a></li>
                     <li class="active-link"><a href="comments.php">Комментарии</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <div class='content' id='content'>
         <div class='container'>
            <form>
               <div class="filters">
                  <select class="form-control" data-table="comments" data-sortField='users.id' data-filter="true">
                     <option data-id="comm_ph.id_us">Аптеки</option>
                     <option data-id="comm_drug.id_us">Медикаменты</option>
                     <option data-id="comm_ph.comm_date">Дата</option>
                  </select>
                  <div id="custom-search-input">
                     <div class="input-group col-md-12">
                        <input type="text" class="form-control" placeholder="Поиск по 'Комментарии'" />
                        <span class="input-group-btn">
                        <button class="btn" type="button">
                        <i class="glyphicon glyphicon-search"></i>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </form>
            <div id='refreshData' class='row'>
               <?php
                  $object = new ConnectDB();
                  $q = "SELECT * FROM users, comm_drug, comm_ph WHERE comm_ph.id_us=users.id";
                  $res = $object->makeQuery($q);  
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="2"){
                        echo "
                           <div class='col-md-12'>
                              <div class='card-comments'>
                                 <div class='img-avatar'>
                                    <img src='../../img/1.jpg'>
                                       <table>
                                          <tr><td>".$mydata[$i]['name']."</td></tr>
                                          <tr><td>".$mydata[$i]['login']."</td></tr>
                                          <tr><td>Комментарий к ".$mydata[$i]['name']."</td></tr>
                                          <tr><td>Дата и время комментария: ".$mydata[$i]['comm_date']."</td></tr>
                                       </table>
                                 </div>
                                 <button class='icon_button_comments' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i></button>
                                 <div class='text_comments'>
                                    <textarea readonly>".$mydata[$i]['comm']."</textarea>
                                 </div> 
                              </div>
                           </div>
                        ";
                     }
                  }
               ?>
            </div>
         </div>
         <!--Пагинация-->
         <nav class="nav-pager">
            <ul class="pagination">
               <li><a href="#">&laquo;</a></li>
               <li class="active"><a href="#">1</a></li>
               <li><a href="#">2</a></li>
               <li><a href="#">3</a></li>
               <li><a href="#">4</a></li>
               <li><a href="#">5</a></li>
               <li><a href="#">&raquo;</a></li>
            </ul>
         </nav>
      </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="../../js/bootstrap.min.js"></script>
      <script src="main.js"></script>
      <script type="text/javascript" src="../../js/script.js"></script>
   </body>
</html>