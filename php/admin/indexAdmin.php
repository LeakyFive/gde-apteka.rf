<?php
   require_once "../db.php";
   session_start();
   ?>
<!DOCTYPE html>
<!--[if It IE 7]<html lang="ru" class ="It-ie8 It-ie7"><![endif]-->
   <!--[if IE 7]<html lang="ru" class ="It-ie9 It-ie8"><![endif]-->
   <!--[if IE 8]<html lang="ru" class ="It-ie9"><![endif]-->
   <!--[if IE 8]><!-->
<html lang="ru">
   <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Панель администратора</title>
      <link rel="shortcut icon" href="../../img/logo.png" type="image/png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" >
      <link rel="stylesheet" href="../../css/bootstrap.min.css">
      <script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
      <link rel="stylesheet" type="text/css" href="style.css">
   </head>
   <body>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <header>
         <nav class="navbar navbar-default" role="navigation">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle </span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="../../img/logo.png"></a>
               </div>
               <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-nav">
                     <li><a href="admins.php">Администраторы</a></li>
                     <li class="active-link"><a href="indexAdmin.php">Аптеки</a></li>
                     <li><a href="users.php">Пользователи</a></li>
                     <li><a class="exit">Выйти</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <div class='content' id='content'>
      <div class='container'>
      <div class="row">
      <div class='btn-add'>
         <button type="button" class="btn btn-success hideadd-button">+ Добавить</button>
      </div>
      </div>
      <form>
         <div class="filters">
            <select class="form-control" data-sortField='region_id' data-table="pharmacy" data-filter="true">
            <?php
               $object = new ConnectDB();
               $res=$object->selectFromTwoTables("city", "region", "region_id");
               $mydata=$res->fetch_all(MYSQLI_ASSOC);
               for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                  echo "
               
                     <option data-sortField='region_id' data-id='".$mydata[$i]['region_id']."'>".$mydata[$i]['city'].", ".$mydata[$i]['region']." </option>
                  ";
               }
            ?>
            </select>
            <select class="form-control" data-sortField='pharmacy.pharm_id' data-table="pharmacy" data-filter="true">
            <?php
               $res=$object->select("pharmacy_types");
               $mydata=$res->fetch_all(MYSQLI_ASSOC);
               for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                  echo "
                     <option data-sortField='pharmacy.pharm_id' data-id='".$mydata[$i]['pharm_id']."'>".$mydata[$i]['pharm_type']." </option>
                       
                  ";
               }
            ?>
            </select>
            <select class="form-control" data-filter="false" data-sortField="name" data-table="pharmacy">
               <option data-sortField="name" data-id="ASC"> Название А-Я </option>
               <option data-sortField="name" data-id="DESC"> Название Я-А </option>
            </select>
         </div>
      </form>
      <div id="add">
      <div class='clearfix' data-id='".$mydata[$i]['id_pharm']."'>
                     <form id="formAdd" method="POST" enctype="multipart/form-data">
                     <div class='col-md-6'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Фото</td><td><input type="file" name="images" required multiple accept=".png, .jpg, .jpeg"></td></tr>
                              <tr><td>Имя</td><td><input type='text' name='name' required placeholder='Имя' value=''></input></td></tr>   
                              <tr><td>Код города</td><td><input type='text' name='id_city' required placeholder='Код города' value=''></input></td></tr>
                              <tr><td>Адрес</td><td><input type='text' name='address' required placeholder='Адрес' value=''></input></td></tr>
                              <tr><td>Часы работы</td><td><input type='text' name='working_hours' required placeholder='Часы работы' value=''></input></td></tr>
                              <tr><td>Телефон</td><td><input type='text' name='phone' required placeholder='Телефон' value=''></input></td></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-6'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Email</td><td><input type='text' name='pharmacy_email' required placeholder='Email' value=''></input></td></tr>
                              <tr><td>Тип аптеки</td><td><input name='pharm_id' required placeholder="Код типа аптеки" type='text' value=''></input></td></tr>   
                              <tr><td>Описание</td><td><input name='description' placeholder="Описание" type='text' value=''></input></td></tr>
                              <tr><td>Рейтинг</td><td><input name='rating' required placeholder="Рейтинг" type='text' value='0'></input></td></tr>
                              <tr><td>Координата X</td><td><input name='coordX' required placeholder="X" type='text' value=''></input></td></tr>
                              <tr><td>Координата Y</td><td><input name='coordY' required placeholder="Y" type='text' value=''></input></td></tr>
                           </table>
                        
                        </div>
                     </div>
                     <input type="submit" data-table='pharmacy' class="btn btn-success add-button" value="Добавить">
                     </form>
      </div>
      </div>
      <div id='refreshData' class='row'>
         <?php
            $object = new ConnectDB();
            $selectQuery = "SELECT * FROM city ,pharmacy,pharmacy_types,photo_pharm WHERE city.id_city=pharmacy.id_city AND pharmacy_types.pharm_id=pharmacy.pharm_id AND photo_pharm.id_pharm=pharmacy.id_pharm";
            $res=$object->makeQuery($selectQuery);
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
            for($i=0,$count = sizeof($mydata);$i<$count;$i++){
               echo "
                                                               
                  <div class='clearfix' data-id='".$mydata[$i]['id_pharm']."'>
                     <div class='col-md-4'>
                        <div class='card'>
                           <img src='../../".$mydata[$i]['photo_patch']."'>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table>
                              <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                              <tr><td><input type='text' data-id='city' disabled value='".$mydata[$i]['city']."'></input></td></tr>
                              <tr><td><input type='text' data-id='address' disabled value='".$mydata[$i]['address']."'></input></td></tr>
                              <tr><td><input type='text' data-id='working_hours' disabled value='".$mydata[$i]['working_hours']."'></input></td></tr>
                              <tr><td><input type='text' data-id='phone' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                              <tr><td><input type='text' data-id='pharmacy_email' disabled placeholder='e-mail отсутствует' value='".$mydata[$i]['pharmacy_email']."'></input></td></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Тип аптеки</td><td><input disabled data-id='pharm_type' type='text' value='".$mydata[$i]['pharm_type']."'></input></td></tr>   
                              <tr><td>Описание</td><td><input disabled data-id='description' type='text' value='".$mydata[$i]['description']."'></input></td></tr>
                              <tr><td>Рейтинг</td><td><input disabled data-id='rating' type='text' value='".$mydata[$i]['rating']."'></input></td></tr>
                              <tr><td>Координата X</td><td><input disabled data-id='coordX' type='text' value='".$mydata[$i]['coordX']."'></input></td></tr>
                              <tr><td>Координата Y</td><td><input disabled data-id='coordY' type='text' value='".$mydata[$i]['coordY']."'></input></td></tr>
                           </table>
                        </div>

                        <div class='update-buttons'>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' type='button' class='btn btn-success ready-button'>Готово</button>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' type='button' class='btn btn-secondary'>Отмена</button>
                        </div>

                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' class='delete-button' data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                     </div>
                  </div>
                  ";
            }
         ?>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="../../js/bootstrap.min.js"></script>
      <script type="text/javascript" src="../../js/script.js"></script>
      <script src="main.js"></script>
   </body>
</html>