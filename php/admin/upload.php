<?php
    require_once "../db.php";

    function reArrayFiles(&$file_post) {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }

    $file_ary = reArrayFiles($_FILES['upload']);
    $object = new ConnectDB();
    foreach ($file_ary as $file) {
        $filePath  = $file['tmp_name'];
        $errorCode = $file['error'];

        $fi = finfo_open(FILEINFO_MIME_TYPE);
        $mime = (string) finfo_file($fi, $filePath);

        finfo_close($fi);

        if (strpos($mime, 'image') === false) die('Можно загружать только изображения.');

        $name = md5_file($filePath);
        $extension = image_type_to_extension(IMAGETYPE_JPEG);
        $format = str_replace('jpeg', 'jpg', $extension);
        $imgPath = __DIR__.'/pics/'.$name.$format;
        $shortImgPath = '/pics/'.$name.$format;
        if (!move_uploaded_file($filePath, $imgPath)) {
            die('При записи изображения на диск произошла ошибка.');
        };
        $q = "INSERT INTO photo_pharm (photo_pharm, id_pharm) VALUES ('$shortImgPath', 2)";
        $object->makeInsert($q);
}
