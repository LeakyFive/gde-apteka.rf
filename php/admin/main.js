// пагинация слайдера
var pos = 0;
var totalSlides = $('.item').length;

$(document).ready(function () {
	var li = document.createElement('li');
	$('#pagination-wrap ul').append(li);
});

countSlides();

pagination();

function countSlides() {
	$('#counter').html(pos + 1 + ' / ' + totalSlides);
}

function pagination() {
	$('#pagination-wrap ul li').removeClass('active');
	$('#pagination-wrap ul li:eq(' + pos + ')').addClass('active');
}


$('#next').click(function () {
	slideRight();
});

//previous slide
$('#previous').click(function () {
	slideLeft();
});


function slideLeft() {
	pos--;
	if (pos == -1) {
		pos = totalSlides - 1;
	}

	countSlides();
	pagination();
}

function slideRight() {
	pos++;
	if (pos == totalSlides) {
		pos = 0;
	}

	countSlides();
	pagination();
}