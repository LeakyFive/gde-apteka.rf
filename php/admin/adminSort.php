<?php                  
   require_once "../db.php";
if ($_GET['table'] == 'admins') {
                  $field = $_GET['field'];
                  $order = $_GET['order'];
                  $object = new ConnectDB();
                  $res=$object->sortBy('users', $field, $order);
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="2"){
                        echo "
                  
                           <div class='border-line col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>
                                    <img class='img img-circle' src='../img/1.jpg'>
                  
                                    <table>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table>
                                 </div>

                                 <div class='icon_button_users'>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i>
                                 </div>
                              </div>
                           </div>
                        </div>                   
                        ";                  
                     }
                  }
}
if ($_GET['table'] == 'moderators') {
   $object = new ConnectDB();
                  $field = $_GET['field'];
                  $order = $_GET['order'];
                  $object = new ConnectDB();
                  $res=$object->sortBy('users', $field, $order);
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="4"){
                        echo "
                           <div class='border-line col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>
                                    <img class='img img-circle' src='../img/1.jpg'>
                  
                                    <table>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table> 
                                 </div>

                                 <div class='icon_button_users'>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i>
                                 </div>
                              </div>
                           </div>
                        ";
                     }
                  }
}
if ($_GET['table'] == 'drug') {
   $object = new ConnectDB();
            $field = $_GET['field'];
            $order = $_GET['order'];
            $object = new ConnectDB();
            $q =  "SELECT drug.name AS drug_name, pharmacy.name AS pharmacy_name, id_categories, price, drug.rating, drug.description, drug.id_drugs FROM drug, pharmacy, drugs_pharm WHERE pharmacy.id_pharm = drugs_pharm.id_pharm AND drug.id_drugs = drugs_pharm.id_drugs ORDER BY $field $order";
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
            for($i=0,$count = sizeof($mydata);$i<$count;$i++){
               echo "
                  <div class='clearfix'>
                     <div class='col-md-4'>
                        <div class='card'>
                           <div class='drug-img'>
                              <img src='../img/1.jpg' alt='...'>
                           </div>
                        </div>
                     </div>
            
                     <div class='col-md-4'>
                        <div class='card'>
                           <table>
                              <tr><td><input type='text' disabled value='".$mydata[$i]['drug_name']."'></input></td></tr>   
                              <tr><td><input type='text' disabled value='".$mydata[$i]['id_categories']."'></input></td></tr>
                              <tr><td><input type='text' disabled value='Цена: ".$mydata[$i]['price']."'></input></td></tr></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Рейтинг</td><td><input type='text' disabled value='".$mydata[$i]['rating']."'></input></td></tr>   
                              <tr><td>Описание</td><td><input disabled type='text' value='".$mydata[$i]['description']."'></input></td></tr>
                              <tr><td>Аптека</td><td><input disabled type='text' value='".$mydata[$i]['pharmacy_name']."'></input></td></tr>
                           </table>
                        </div>

                        <div class='icon_button'>
                           <button data-id='".$mydata[$i]['id_drugs']."'> <i class='glyphicon glyphicon-pencil'></i>
                           <button data-id='".$mydata[$i]['id_drugs']."'> <i class='glyphicon glyphicon-trash'></i>
                        </div>
                     </div>
                  </div>
                  <hr>
               ";
            }
}
if ($_GET['table'] == 'pharmacy') {
            $field = $_GET['field'];
            $order = $_GET['order'];
            $object = new ConnectDB();
            $q = "SELECT * FROM city ,pharmacy,pharmacy_types WHERE city.id_city=pharmacy.id_city AND pharmacy_types.pharm_id=pharmacy.pharm_id ORDER BY $field $order";
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
            for($i=0,$count = sizeof($mydata);$i<$count;$i++){  // parsing changed - fix it!!
               echo "
            
                  <div class='clearfix'>
                     <div class='col-md-4'>
                        <div class='card'>
                           <div id='block-for-slider'>
                              <div id='viewport'>
                                 <div id='slider-pharm_".$mydata[$i]['pharm_type']."' class='carousel slide' data-ride='carousel' data-interval='false'>
            
                                    <div class='carousel-inner'>
                                       <div class='item active'>
                                          <img src='../img/1.jpg' alt='...'>
                                       </div>
                                       <div class='item'>
                                          <img src='../img/1.jpg' alt='...'>
                                       </div>
                                    </div>
            
                                    <a id='previous' class='left carousel-control carous' href='#slider-pharm_".$mydata[$i]['pharm_type']."' role='button' data-slide='prev'>
                                       <i class='glyphicon glyphicon-chevron-left cari'></i>
                                    </a>
                                    <a id='next' class='right carousel-control carous' href='#slider-pharm_".$mydata[$i]['pharm_type']."' role='button' data-slide='next'>
                                       <i class='glyphicon glyphicon-chevron-right cari'></i>
                                    </a>
            
                                    <div class='delete'>            
                                       <i data-id='".$mydata[$i]['pharm_type']."' class='glyphicon glyphicon-remove '></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table>
                              <tr><td><input type='text' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                              <tr><td><input type='text' disabled value='".$mydata[$i]['city'].",".$mydata[$i]['address']."'></input></td></tr>
                              <tr><td><input type='text' disabled value='".$mydata[$i]['working_hours']."'></input></td></tr>
                              <tr><td><input type='text' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                              <tr><td><input type='text' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['pharmacy_email']."'></input></td></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Тип аптеки</td><td><input type='text' disabled value='".$mydata[$i]['pharm_type']."'></input></td></tr>   
                              <tr><td>Описание</td><td><input disabled type='text' value='".$mydata[$i]['description']."'></input></td></tr>
                              <tr><td>Рейтинг</td><td><input disabled type='text' value='".$mydata[$i]['rating']."'></input></td></tr>
                              <tr><td>Геометка</td><td><input disabled type='text' value='".$mydata[$i]['geotag']."'></input></td></tr>
                           </table>
                        </div>

                        <div class='icon_button'>
                           <button data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-pencil'></i>
                           <button data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-trash'></i>
                        </div>
                     </div>
                  </div>
                  <hr>
               ";
            }
}
if ($_GET['table'] == 'users') {
   $object = new ConnectDB();
                  $field = $_GET['field'];
                  $order = $_GET['order'];
                  $object = new ConnectDB();
                  $res=$object->sortBy('users', $field, $order);
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="1"){
                        echo "
                  
                           <div class='border-line col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>
                                    <img class='img img-circle' src='../img/1.jpg'>
                  
                                    <table>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='text' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table>
                                 </div>

                                 <div class='icon_button_users'>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i>
                                    <button data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i>
                                 </div>
                              </div>
                           </div>
                        </div>                   
                        ";                  
                     }
                  }
}
if ($_GET['table'] == 'comments') {
                  $object = new ConnectDB();
                  $q = "SELECT * FROM users, comm_drug WHERE comm_drug.id_us=users.id";
                  $res = $object->makeQuery($q);
                  $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                     if($mydata[$i]['id_type']=="2"){
                        echo "
                           <div class='col-md-12'>
                              <div class='card-comments'>
                                 <div class='img-avatar'>
                                    <img src='../img/1.jpg'>
                                       <table>
                                          <tr><td>".$mydata[$i]['name']."</td></tr>
                                          <tr><td>".$mydata[$i]['login']."</td></tr>
                                          <tr><td>Комментарий к ".$mydata[$i]['name']."</td></tr>
                                          <tr><td>Дата и время комментария: ".$mydata[$i]['comm_date']."</td></tr>
                                       </table>
                                 </div>
                                 <button class='icon_button_comments' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i></button>
                                 <div class='text_comments'>
                                    <textarea readonly>".$mydata[$i]['comm']."</textarea>
                                 </div> 
                              </div>
                           </div>
                        ";
                     }
                  }
}
               ?>