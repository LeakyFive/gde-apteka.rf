<?php
   require_once "../db.php";
   session_start();
   ?>
<!DOCTYPE html>
<!--[if It IE 7]<html lang="ru" class ="It-ie8 It-ie7"><![endif]-->
   <!--[if IE 7]<html lang="ru" class ="It-ie9 It-ie8"><![endif]-->
   <!--[if IE 8]<html lang="ru" class ="It-ie9"><![endif]-->
   <!--[if IE 8]><!-->
<html lang="ru">
   <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Панель администратора</title>
      <link rel="shortcut icon" href="../../img/logo.png" type="image/png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- совместимость с IE -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--для адаптивной работы сайта-->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" >
      <link rel="stylesheet" href="../../css/bootstrap.min.css">
      <script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
      <link rel="stylesheet" type="text/css" href="style.css">
   </head>
   <body>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <header>
         <nav class="navbar navbar-default" role="navigation">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle </span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="../../img/logo.png"></a>
               </div>
               <!--Меню-->
               <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-nav">
                     <li><a href="admins.php">Администраторы</a></li>
                     <li><a href="moderators.php">Модераторы</a></li>
                     <li><a href="indexAdmin.php">Аптеки</a></li>
                     <li  class="active-link"><a href="drugs.php">Медикаменты</a></li>
                     <li><a href="users.php">Пользователи</a></li>
                     <li><a href="comments.php">Комментарии</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <div class='content' id='content'>
      <div class='container'>
      <form>
         <div class="filters">
            <select class="form-control" data-table="drug" data-sortField='id_categories' data-filter="true">
            <?php
               $object = new ConnectDB();
               $res=$object->select("drugs_catecories");
               $mydata=$res->fetch_all(MYSQLI_ASSOC);
               for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                  echo "
                     <option data-id='".$mydata[$i]['id_categories']."'>".$mydata[$i]['category']." </option>
                  ";
               }
            ?>
            </select>
            <select class="form-control" data-table="drug" data-sortField="drug_name" data-filter="false">
               <option data-sortField="drug_name" data-id="ASC"> Название А-Я </option>
               <option data-sortField="drug_name" data-id="DESC"> Название Я-А </option>
            </select>
            <select class="form-control" data-table="drug" data-sortField="price" data-filter="false">
               <option data-sortField="price" data-id="ASC"> Цена ↑  </option>
               <option data-sortField="price" data-id="DESC"> Цена ↓  </option>
            </select>
            <div id="custom-search-input">
               <div class="input-group col-md-12">
                  <input type="text" class="form-control" placeholder="Поиск по 'Медикаменты'" />
                  <span class="input-group-btn">
                  <button class="btn" type="button">
                  <i class="glyphicon glyphicon-search"></i>
                  </button>
                  </span>
               </div>
            </div>
         </div>
      </form>
      <div id='refreshData' class='row'>
         <?php
            $object = new ConnectDB();
            $q =  "SELECT drug.name AS drug_name, pharmacy.name AS pharmacy_name, id_categories, price, drug.rating, drug.description, drug.id_drugs FROM drug, pharmacy, drugs_pharm WHERE pharmacy.id_pharm = drugs_pharm.id_pharm AND drug.id_drugs = drugs_pharm.id_drugs";
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
            for($i=0,$count = sizeof($mydata);$i<$count;$i++){
               echo "
                  <div class='clearfix'>
                     <div class='col-md-4'>
                        <div class='card'>
                           <div class='drug-img'>
                              <img src='../../img/1.jpg' alt='...'>
                           </div>
                        </div>
                     </div>
            
                     <div class='col-md-4'>
                        <div class='card'>
                           <table>
                              <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['drug_name']."'></input></td></tr>   
                              <tr><td><input type='text' data-id='id_categories' disabled value='".$mydata[$i]['id_categories']."'></input></td></tr>
                              <tr><td>Цена: <input type='text'data-id='price' disabled value='".$mydata[$i]['price']."'></input></td></tr></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Рейтинг</td><td><input type='text' data-id='rating' disabled value='".$mydata[$i]['rating']."'></input></td></tr>   
                              <tr><td>Описание</td><td><input data-id='description' disabled type='text' value='".$mydata[$i]['description']."'></input></td></tr>
                              <tr><td>Аптека</td><td><input data-id='pharmacy_name' disabled type='text' value='".$mydata[$i]['pharmacy_name']."'></input></td></tr>
                           </table>
                        </div>
                        <div class='update-buttons'>
                           <button data-table='drug' data-idfield='id_drugs' data-id='".$mydata[$i]['id_drugs']."' type='button' class='btn btn-success ready-button'>Готово</button>
                           <button data-table='drug' data-idfield='id_drugs' data-id='".$mydata[$i]['id_drugs']."' type='button' class='btn btn-secondary'>Отмена</button>
                        </div>
                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id_drugs']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='drug' data-idfield='id_pharm' data-id='".$mydata[$i]['id_drugs']."' class='delete-button' data-id='".$mydata[$i]['id_drugs']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                     </div>
                  </div>
                  <hr>
               ";
            }
         ?>
         <!--Пагинация-->
         <nav class="nav-pager">
            <ul class="pagination">
               <li><a href="#">&laquo;</a></li>
               <li class="active"><a href="#">1</a></li>
               <li><a href="#">2</a></li>
               <li><a href="#">3</a></li>
               <li><a href="#">4</a></li>
               <li><a href="#">5</a></li>
               <li><a href="#">&raquo;</a></li>
            </ul>
         </nav>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="../../js/bootstrap.min.js"></script>
      <script src="main.js"></script>
      <script type="text/javascript" src="../../js/script.js"></script>
   </body>
</html>