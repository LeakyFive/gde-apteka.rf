<?php                  

require_once "db.php";

$data = json_decode($_GET['data'], true);

if ($data[0]['table'] == 'admins') {
   $object = new ConnectDB();
            $q = "SELECT * FROM users WHERE id_type=2";
            for ($i=0; $i < sizeof($data); $i++) { 
               if ($data[$i]['field'] == 'name') {
                  $orderField = $data[$i]['field'];
                  $order = $data[$i]['currentValue'];
                  $q .= " ORDER BY $orderField $order";
               }
            }
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                        echo "
                           <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6' data-id='".$mydata[$i]['id']."'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>

                                    <img class='img img-circle' src='../../img/user-icon.png'>
                  
                                    <table>
                                       <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' data-id='login' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='password' data-id='password' disabled value='".$mydata[$i]['password']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='phone' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='email' disabled placeholder='e-mail отсутствует' value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table>
                                 </div>
                                 <div class='update-buttons'>
                           <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-success ready-button glyphicon glyphicon-ok'></button>
                           <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-secondary glyphicon glyphicon-remove'></button>
                        </div>

                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' class='delete-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                              </div>
                           </div>                 
                        ";                  
                     
                  }
}

if ($data[0]['table'] == 'users') {
   $object = new ConnectDB();
            $q = "SELECT * FROM users WHERE id_type=1";
            for ($i=0; $i < sizeof($data); $i++) { 
               if ($data[$i]['field'] == 'login') {
                  $orderField = $data[$i]['field'];
                  $order = $data[$i]['currentValue'];
                  $q .= " ORDER BY $orderField $order";
               }
            }
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
                  for($i=0,$count = sizeof($mydata);$i<$count;$i++){
                        echo "
                           <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                              <div class='card-admin'>
                                 <div class='img-avatar'>
                                    <img class='img img-circle' src='../../img/user-icon.png'>
                                    <table>
                                       <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                                       <tr><td><input type='text' data-id='login' disabled value='".$mydata[$i]['login']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='phone' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                                       <tr><td><input type='text' data-id='email' disabled placeholder='e-mail отсутствует'value='".$mydata[$i]['email']."'></input></td></tr>
                                    </table>
                                 </div>
                                 <div class='update-buttons'>
                                    <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-success ready-button glyphicon glyphicon-ok'></button>
                                    <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' type='button' class='btn btn-secondary glyphicon glyphicon-remove'></button>
                                 </div>
                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='users' data-idfield='id' data-id='".$mydata[$i]['id']."' class='delete-button' data-id='".$mydata[$i]['id']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                              </div>
                           </div>                
                        ";                  
                     
                  }
}

if ($data[0]['table'] == 'pharmacy') {
			$object = new ConnectDB();
            $q = "SELECT * FROM city ,pharmacy,pharmacy_types,photo_pharm WHERE city.id_city=pharmacy.id_city AND pharmacy_types.pharm_id=pharmacy.pharm_id AND photo_pharm.id_pharm=pharmacy.id_pharm";
            for ($i=0; $i < sizeof($data); $i++) { 
            	if (isset($data[$i]['currentValue']) && $data[$i]['filter'] == 'true') {
            		$field = $data[$i]['field'];
            		$value = $data[$i]['currentValue'];
            		$q .= " AND $field=$value";
            	}
            	if ($data[$i]['field'] == 'name') {
            		$orderField = $data[$i]['field'];
            		$order = $data[$i]['currentValue'];
            		$q .= " ORDER BY $orderField $order";
            	}
            }
            //var_dump($q);
            $res = $object->makeQuery($q);   
            $mydata=$res->fetch_all(MYSQLI_ASSOC);
            for($i=0,$count = sizeof($mydata);$i<$count;$i++){
               echo "
            
                  <div class='clearfix' data-id='".$mydata[$i]['id_pharm']."'>
                     <div class='col-md-4'>
                        <div class='card'>
                           <img src='../../".$mydata[$i]['photo_patch']."'>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table>
                              <tr><td><input type='text' data-id='name' disabled value='".$mydata[$i]['name']."'></input></td></tr>   
                              <tr><td><input type='text' data-id='city' disabled value='".$mydata[$i]['city']."'></input></td></tr>
                              <tr><td><input type='text' data-id='address' disabled value='".$mydata[$i]['address']."'></input></td></tr>
                              <tr><td><input type='text' data-id='working_hours' disabled value='".$mydata[$i]['working_hours']."'></input></td></tr>
                              <tr><td><input type='text' data-id='phone' disabled value='".$mydata[$i]['phone']."'></input></td></tr>
                              <tr><td><input type='text' data-id='pharmacy_email' disabled placeholder='e-mail отсутствует' value='".$mydata[$i]['pharmacy_email']."'></input></td></tr>
                           </table>
                        </div>
                     </div>
                     <div class='col-md-4'>
                        <div class='card'>
                           <table class='table table-bordered'>
                              <tr><td>Тип аптеки</td><td><input disabled data-id='pharm_type' type='text' value='".$mydata[$i]['pharm_type']."'></input></td></tr>   
                              <tr><td>Описание</td><td><input disabled data-id='description' type='text' value='".$mydata[$i]['description']."'></input></td></tr>
                              <tr><td>Рейтинг</td><td><input disabled data-id='rating' type='text' value='".$mydata[$i]['rating']."'></input></td></tr>
                              <tr><td>Координата X</td><td><input disabled data-id='coordX' type='text' value='".$mydata[$i]['coordX']."'></input></td></tr>
                              <tr><td>Координата Y</td><td><input disabled data-id='coordY' type='text' value='".$mydata[$i]['coordY']."'></input></td></tr>
                           </table>
                        </div>

                        <div class='update-buttons'>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' type='button' class='btn btn-success ready-button'>Готово</button>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' type='button' class='btn btn-secondary'>Отмена</button>
                        </div>

                        <div class='icon_button'>
                           <button class='update-button' data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-pencil'></i></button>
                           <button data-table='pharmacy' data-idfield='id_pharm' data-id='".$mydata[$i]['id_pharm']."' class='delete-button' data-id='".$mydata[$i]['id_pharm']."'> <i class='glyphicon glyphicon-trash'></i></button>
                        </div>
                     </div>
                  </div>
                  
               ";
            }
}

