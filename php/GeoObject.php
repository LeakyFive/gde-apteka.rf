<?php


class FeatureCollection
{
	public $type = 'FeatureCollection';
	public $features; 
}

class Features
{
	public $type = 'Feature';
	public $id; 
	public $geometry;
	public $properties;
}

class Geometry
{
	public $type = 'Point';
	public $coordinates; 
}

class Properties
{
	public $balloonContentHeader; //заголовок метки на карте
	public $balloonContentBody; //тело метки на карте
	// public $balloonContentFooter; //футер метки на карте
}