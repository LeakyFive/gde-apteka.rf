<?php
	include "../php/db.php";
	session_start();
	$connect = new ConnectDB();
			$id = $_SESSION['id'];
			$rec = $connect->makeQuery("SELECT * FROM users,types WHERE id = '$id' AND types.id_type = users.id_type");
			$data = $rec->fetch_assoc();
	if ($_SESSION['type'] == 'admin') {
		header("Location: ../php/admin/indexAdmin.php");
	}
?>

<!DOCTYPE html>
<html lang="ru">
	<head>
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="../fonts/fonts.css">
		<link href="css.css" rel="stylesheet" type="text/css" />
		<script src="../js/jquery-3.3.1.js"></script>
	</head>
	<title>Личный кабинет</title>
	<body>
	<section>
        <a href="../index.php"><img class="logo" src="../img/logoIndex.png" alt="Где-аптека.рф" /></a>
		<form action="../php/log_out.php" method="post" enctype="multipart/form-data">
			<h4>Пользователь</h4>
			<input autocomplete="on" type='text' size="25" pattern="[А-Яа-яЁё]{,10}" placeholder="Имя" name="name" value="<?=$data['name']?>">
			<br>
			<input autocomplete="on" type='tel' size="25" pattern="+7\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}" placeholder="Телефон" name="phone" value="<?=$data['phone']?>"><br>
			<input autocomplete="on" type='text' size="25" placeholder="Email" name="email" value="<?=$data['email']?>"><br>
			<input autocomplete="off" type='text' pattern="[A-Za-zА-Яа-яЁё]{6,12}" size="25" placeholder="Логин" name="login" maxlength="14" value="<?=$data['login']?>"><br>
			<input autocomplete="off" type='password' size="25" placeholder="Пароль" name="password"  maxlength="14" value="<?=$data['password']?>"><br>
			<button type='button' name="edit" size="25"  >Сохранить изменения</button>
			<p class="error"></p>
			<button type='button' name="exit" size="25"  >Выйти</button>
		</form>
	</section>
	<script src="../js/scriptLk.js"></script>
	</body>
</html>