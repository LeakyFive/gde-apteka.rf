<?php
session_start();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Где-аптека.рф</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/jquery-3.3.1.js"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
	<main>
	<header class="header">
		<div class="header-wrapper">
			<a href="#"><img class="logo" src="img/logoIndex.png" alt="Где-аптека.рф" /></a>
			<nav class="navbar">
			<ul>
				<li class="navbar-item"><a href="partners.html">Партнёрство</a></li>
				<?php if (isset($_SESSION['id'])) {
				echo "
				<li class='navbar-item'><a href='forms/lk.php' class='navbar-item-reg'>Личный кабинет</a>
				"; } else {
				echo "
				<li class='navbar-item'><a href='forms/login.html'>Войти</a></li>
				<li class='navbar-item'><a href='forms/index.html' class='navbar-item-reg'>Регистрация</a>";
				} ?> 
			</ul>
			</nav>			
		</div>
	</header>
	<section class="search-section">
		<div class="search-wrapper">
			<h1 class="search-title">В нужный момент, всегда под рукой</h1>
			<form class="search-form">
				<input type="text" placeholder="Введите название аптеки или лекарства" class="search-field" required name="search-field">
				<input type="button" class="search-button" value="Найти" name="search-button">
			</form>
		</div>
	</section>	
	<footer class="footer">
		<div class="footer-wrapper">
			<p class="copyright">&copy; Где-аптека.рф 2017</p>
			<div class="policy">
				<a href="politikaconf.pdf">Политика конфиденциальности</a>
			</div>
			</div>
		</div>
	</footer>
</main>
	<script src="js/scriptIndex.js"></script>
</body>
</html>

